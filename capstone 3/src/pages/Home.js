import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import BootstrapCarousel from '../components/BootstrapCarousel'
export default function Home() {
	
	const data = {
	}

	return (
		<>
	
		   <BootstrapCarousel />
		    <Banner data={data} />
   		  <Highlights />
		</>

	)

}
 