import {useState, useEffect, useContext} from 'react';

import {Navigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from "sweetalert2";

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
export default function Register() {

  const {user, setUser} = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);


  function registerUser(e){
    e.preventDefault(); 
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {

      if(data === true){

        Swal.fire({
            title: "Email Already Exist",
            icon: "error",
            text: "Kindly provide another email to complete registration.",
          });



      } else {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
          method: "POST",
          headers:{
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({

            email: email,
            password: password1

          })
        })
        .then(res => res.json())
        .then(data => {

          if(data === true){

              setEmail ("")
              setPassword1 ("")

          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Gabzki Music store",
          })

          } else { // not successful
            alert("Please try again. Something went wrong");
          }
        })
      }

    })

}



 useEffect(() => {
      if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
        setIsActive(true);
      } else {
        setIsActive(false);
  }

  }, [email, password1, password2])

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    return (
      (user.id !== null) ? 
      <Navigate to="/Login" />
      :

        <Form onSubmit={(e) => registerUser(e)}>

          <h2 className="mt-5 text-center">Register</h2>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                  type="email" 
                  placeholder="Enter email" 
                  value={email}
                  onChange = {e => setEmail(e.target.value)}
                  required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Password" 
                   value={password1}
                  onChange = {e => setPassword1(e.target.value)}
                  required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Verify Password" 
                  value={password2}
                  onChange = {e => setPassword2(e.target.value)}
                  required
                />
            </Form.Group>

              { isActive ? 
                 <Button variant="primary" type="submit" id="submitBtn">
              Submit
                 </Button>
                 :
                 <Button variant="danger" type="submit" id="submitBtn" disabled>
              Submit
                 </Button>
                }

           
        </Form>
    )

}
    