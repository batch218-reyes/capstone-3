import ProductCard from '../components/ProductCard';

import AdminView from '../components/AdminView';
import UserContext from '../UserContext';
import {Button,Row, Col } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
export default function Products() {
	const [productData, setProductData] = useState([]);
	const {user}= useContext(UserContext)
	console.log(user)

	
	 console.log(productData);
	 console.log(productData[0]);

const fetchData= ()=>{
	fetch(`${process.env.REACT_APP_API_URL}/products/allproducts`)
	.then(res => res.json())
	.then(data => {
	console.log(data)
	setProductData(data)
	console.log(data)
	})
}
	useEffect(() =>{
		fetchData()
	},[])


//----------------------------------------------------------------
	
	
const product =productData.map(product =>{
		if(product.isActive){
			return(
				<ProductCard productsProp={product} key={product._id} />
			)
		}else{
			return null
		}
	})

	return(
			(user.isAdmin)?
		    <AdminView productsProp={productData}useEffect={useEffect}/>
		:
		<>
		<Row>
			<Col className="p-5 text-center">
				<h1 className="me-auto">Products</h1>
			</Col>	
        </Row>  

		{product}
		</>
	)
}
