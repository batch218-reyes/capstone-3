import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
export default function AdminView(props){
	
	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [productName, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const [showEdit, setShowEdit] = useState(false)
	const [showEdit2, setShowEdit2]= useState(false)
	const token = localStorage.getItem("token")

	const openEdit = (productId) => {
		console.log(productId)
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
	
			setProductId(data._id)
			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowEdit(true)
	}

	const openEdit2 = () => {
			setProductId()
			setName()
			setDescription()
			setPrice()
		

		setShowEdit2(true)
	}

const closeEdit = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}
	const closeEdit2 = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit2(false)
	}

//--------------update a product---------------------------------------------------------
	const editProduct = (e) => {
		e.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
                    title: "Product Updates",
                    icon: "success",

                })
            } 

             else {
                    Swal.fire({
                    title: "Failed to Update Product",
                    icon: "error",
                    text: "You are not allowed to update Product Details"
                })

            };
		})
	}

	//-----------------ADD PRODUCT -------------------------------------------------------------------


	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${token}`
					},
					body: JSON.stringify({
						productName: productName,
						description: description,
						price: price
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						alert("added product")

						closeEdit2()
						fetchData()
							
					}else{
						alert("Something went wrong")
					}
				})
			}

//----------------END OF ADD PRODUCT----------------------------------------------------------------------

//-----------------START OF ARCHIVE PRODUCT---------------------------------------------------------------

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				alert(`Product successfully ${bool}`)

				fetchData()
			}else{
				alert("Something went wrong")
			}
		})
	}

		const activateToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
		

                    Swal.fire({
                            title: "Item launched!",
                            icon: "success",
                            text: "Happy selling!"
                        })

                        navigate("/products");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
      
                })
            }

		})
	}
//---------------------------------------------------------------------------------------------------------

	useEffect(() => {
		//map through the ProductsProp to generate table contents
		const products = productsProp.map(products => {
			return(
				<tr key={products._id}>
					<td>{products.productName}</td>
					<td>{products.description}</td>
					<td>{products.price}</td>
					<td>
							{/*Dynamically render products
							availability*/}
							{products.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(products._id)}>Update</Button>
						{products.isActive
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(products._id, products.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => activateToggle(products._id, products.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp])
	
	return(
		<>
			<h2>Admin Dashboard</h2>

			{/*Course info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the productsProp*/}
					{productsArr}
				</tbody>
			</Table>

			<Button variant="warning" size="sm" onClick={() => openEdit2()}>add product</Button>


			{/*Edit product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={productName}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			{/*modal for add product*/}
			<Modal show={showEdit2} onHide={closeEdit2}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>add product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={productName}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit2}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>





		</>
	)
}
