import {useState, useEffect} from 'react';
import { Row, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';



//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

export default function ProductCard({productsProp}) {



  const {_id, productName, description, price} = productsProp;

  function addToCart() { 

  }
    return (

  <Card className= "mb-2 " >
      <Card.Body className="d-flex flex-column">
          <Card.Title className="d-flex justify-content-between align-items-baseline mb-4"></Card.Title>
          <span className="fs-2">{productName}</span>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
           <span>
              <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
          </span>
          <span>    
          </span>   
      </Card.Body>
  </Card>
  )
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    productNameame: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}