import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Gabriel Music Store</h1>
            <p>Find your passion!</p>
            <Button variant="primary">Shop now!</Button>
        </Col>
    </Row>
	)
}