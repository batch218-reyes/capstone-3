import {useState, useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';
import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar() {

  //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  const {user} = useContext(UserContext);


  return (
  <Navbar bg="dark" variant="dark" expand="lg">
        <Container>
            <Navbar.Brand as={Link} to="/"> Gabriel Music Store </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
           
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/Products">Products</Nav.Link>

            {
              (user.id !== null) ? 
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
              
            }
          </Nav>
        </Navbar.Collapse>
        </Container>
    </Navbar>
  );
}

